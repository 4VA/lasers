/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.lasers;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ShapedRecipe;

public abstract class Recipes {

	public static ShapedRecipe	LASER_CRYSTAL;
	public static ShapedRecipe	LASER_EMITTER;
	public static ShapedRecipe	LASER_RECEIVER;
	public static ShapedRecipe	MIRROR_ROTATOR;

	protected static void load() {
		/* Laser Crystal */
		LASER_CRYSTAL = new ShapedRecipe(Items.LASER_CRYSTAL);
		LASER_CRYSTAL.shape("rrr", "rdr", "rrr");
		LASER_CRYSTAL.setIngredient('r', Material.REDSTONE);
		LASER_CRYSTAL.setIngredient('d', Material.DIAMOND);

		/* Laser Emitter */
		LASER_EMITTER = new ShapedRecipe(Items.LASER_EMITTER);
		LASER_EMITTER.shape("sgs", "scs", "sts");
		LASER_EMITTER.setIngredient('s', Material.STONE);
		LASER_EMITTER.setIngredient('g', Material.GLASS);
		LASER_EMITTER.setIngredient('c', Material.DIAMOND);// Laser Crystal
		LASER_EMITTER.setIngredient('t', Material.REDSTONE_TORCH_ON);

		/* Laser Receiver */
		LASER_RECEIVER = new ShapedRecipe(Items.LASER_RECEIVER);
		LASER_RECEIVER.shape("sgs", "scs", "sts");
		LASER_RECEIVER.setIngredient('s', Material.STONE);
		LASER_RECEIVER.setIngredient('g', Material.GLASS);
		LASER_RECEIVER.setIngredient('c', Material.DIAMOND);// Laser Crystal
		LASER_RECEIVER.setIngredient('t', Material.REDSTONE_COMPARATOR);

		/* Mirror Rotator */
		MIRROR_ROTATOR = new ShapedRecipe(Items.MIRROR_ROTATOR);
		MIRROR_ROTATOR.shape("rpr", "ptp", "cpc");
		MIRROR_ROTATOR.setIngredient('r', Material.REDSTONE);
		MIRROR_ROTATOR.setIngredient('p', Material.PISTON_BASE);
		MIRROR_ROTATOR.setIngredient('t', Material.REDSTONE_TORCH_ON);
		MIRROR_ROTATOR.setIngredient('c', Material.REDSTONE_COMPARATOR);

		inject();
	}

	protected static void inject() {
		Bukkit.addRecipe(LASER_CRYSTAL);
		Bukkit.addRecipe(LASER_EMITTER);
		Bukkit.addRecipe(LASER_RECEIVER);
		Bukkit.addRecipe(MIRROR_ROTATOR);
	}

}
