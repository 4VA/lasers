/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.lasers;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Dispenser;
import org.bukkit.block.Dropper;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

public class LasersListener implements Listener {

	@EventHandler
	public void onCraftItem(CraftItemEvent e) {
		if (e.getInventory() != null) {
			final CraftingInventory inv = e.getInventory();
			if (inv.getSize() == 10) {
				if (e.getRecipe().getResult().equals(Items.LASER_EMITTER) || e.getRecipe().getResult().equals(Items.LASER_RECEIVER)) {
					ItemStack crystal = inv.getMatrix()[4];
					if (!Items.LASER_CRYSTAL.isSimilar(crystal)) {
						inv.setResult(new ItemStack(Material.AIR));
						e.setCancelled(true);
						e.setResult(Result.DENY);
					}
				}
			}
		}
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		if (e.isCancelled()) { return; }
		if (e.getClickedBlock() == null) { return; }
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (Items.LASER_EMITTER.getType() == e.getClickedBlock().getType() || Items.LASER_RECEIVER.getType() == e.getClickedBlock().getType() || Items.MIRROR_ROTATOR.getType() == e.getClickedBlock().getType()) {
				if (e.getClickedBlock().hasMetadata("Lasers")) {
					e.setUseInteractedBlock(Result.DENY);
					e.setUseItemInHand(Result.ALLOW);
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockPlace(BlockPlaceEvent e) {
		if (e.isCancelled() || !e.canBuild()) { return; }
		if (Items.LASER_CRYSTAL.equals(e.getItemInHand())) {
			e.setBuild(false);
			e.setCancelled(true);
			return;
		}
		if (e.getBlock() == null || e.getBlock().getType() == Material.AIR) { return; }
		if (Items.LASER_EMITTER.isSimilar(e.getItemInHand())) {
			e.getBlock().setMetadata("Lasers", new FixedMetadataValue(Lasers.instance, Items.LASER_EMITTER));
			LaserRunnable.lasers.add(e.getBlock());
			if (e.getBlock().getState() instanceof InventoryHolder) {
				((InventoryHolder) e.getBlock().getState()).getInventory().addItem(new ItemStack(Material.WEB, 64, (short) 1));
			}
		}
		if (Items.LASER_RECEIVER.isSimilar(e.getItemInHand())) {
			e.getBlock().setMetadata("Lasers", new FixedMetadataValue(Lasers.instance, Items.LASER_RECEIVER));
			LaserRunnable.lasers.add(e.getBlock());
		}
		if (Items.MIRROR_ROTATOR.isSimilar(e.getItemInHand())) {
			e.getBlock().setMetadata("Lasers", new FixedMetadataValue(Lasers.instance, Items.MIRROR_ROTATOR));
			e.getBlock().setData((byte) 1);// Face up (off)
			LaserRunnable.lasers.add(e.getBlock());
			if (e.getBlock().getState() instanceof InventoryHolder) {
				((InventoryHolder) e.getBlock().getState()).getInventory().addItem(new ItemStack(Material.WEB, 64, (short) 1));
			}
		}
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		if (e.isCancelled()) { return; }
		if (e.getBlock() == null) { return; }
		if (e.getBlock().hasMetadata("Lasers")) {
			ItemStack toDrop = null;
			if (e.getBlock().getType() == Items.LASER_EMITTER.getType() || e.getBlock().getType() == Items.LASER_RECEIVER.getType() || e.getBlock().getType() == Items.MIRROR_ROTATOR.getType()) {
				for (MetadataValue meta : e.getBlock().getMetadata("Lasers")) {
					if (meta.value() instanceof ItemStack) {
						toDrop = (ItemStack) meta.value();
						break;
					}
				}
			}
			if (toDrop != null) {
				if (e.getBlock().getState() instanceof InventoryHolder) {
					((InventoryHolder) e.getBlock().getState()).getInventory().clear();
				}
				LaserRunnable.activeLasers.remove(e.getBlock());
				LaserRunnable.lasers.remove(e.getBlock());
				e.getBlock().setType(Material.AIR);
				if (e.getPlayer().getGameMode() != GameMode.CREATIVE) {
					e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), toDrop);
				}
			}
		}
	}

	@EventHandler
	public void onInventoryMove(InventoryMoveItemEvent e) {
		InventoryHolder holder = e.getSource().getType() == InventoryType.DISPENSER || e.getSource().getType() == InventoryType.DROPPER ? e.getSource().getHolder() : e.getDestination().getHolder();
		if (holder == null) { return; }
		Block block = null;
		if (holder instanceof Dispenser) {
			block = ((Dispenser) holder).getBlock();
		}
		if (holder instanceof Dropper) {
			block = ((Dropper) holder).getBlock();
		}
		if (block == null) { return; }
		if (Items.LASER_EMITTER.getType() == block.getType() || Items.LASER_RECEIVER.getType() == block.getType()) {
			if (block.hasMetadata("Lasers")) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onDispense(BlockDispenseEvent e) {
		if (e.getBlock() == null) { return; }
		if (e.getBlock().hasMetadata("Lasers")) {
			ItemStack metaItem = null;
			for (MetadataValue meta : e.getBlock().getMetadata("Lasers")) {
				if (meta.value() instanceof ItemStack) {
					metaItem = (ItemStack) meta.value();
					break;
				}
			}
			if (Items.LASER_EMITTER.getType() == e.getBlock().getType() && Items.LASER_EMITTER.isSimilar(metaItem)) {
				LaserRunnable.activeLasers.add(e.getBlock());
				e.setCancelled(true);
				e.setItem(new ItemStack(Material.AIR));
			}
			if (Items.LASER_RECEIVER.getType() == e.getBlock().getType() && Items.LASER_RECEIVER.isSimilar(metaItem)) {
				e.setCancelled(true);
				e.setItem(new ItemStack(Material.AIR));
			}
			if (Items.MIRROR_ROTATOR.getType() == e.getBlock().getType() && Items.MIRROR_ROTATOR.isSimilar(metaItem)) {
				e.setCancelled(true);
				e.setItem(new ItemStack(Material.AIR));
				Block up = e.getBlock().getRelative(BlockFace.UP);
				if (e.getBlock().getData() != 1 && e.getBlock().getData() != 9) {// 1 = off, 9 = on
					Lasers.instance.getLogger().warning("A invalid MirrorRotator was triggered but has a data value of " + e.getBlock().getData() + "!");
					return;
				}
				if (up.getTypeId() == 176) {
					byte newData = Lasers.bannerHelper.getBannerAngleByPower(up);
					if (newData != up.getData()) {
						up.getWorld().playSound(e.getBlock().getLocation(), Sound.BLOCK_PISTON_EXTEND, 1, 1);
						up.setData(newData);
						e.getBlock().setData((byte) 1);// Turn the dispenser off, so it can be triggered again
						up.getWorld().playSound(e.getBlock().getLocation(), Sound.BLOCK_PISTON_CONTRACT, 1, 1);
					}
				}
			}
		}
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		if (e.getEntity().hasMetadata("Laser_Damage")) {
			for (MetadataValue meta : e.getEntity().getMetadata("Laser_Damage")) {
				if (meta.asDouble() == Lasers.LASER_DAMAGE_AMOUNT) {
					e.setDeathMessage(Lasers.DEATH_MESSAGE.replace("%player%", e.getEntity().getName()));
					e.getEntity().removeMetadata("Laser_Damage", Lasers.instance);
					break;
				}
			}
		}
	}
}
